package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Customer;
import com.example.demo.Model.Drink;
import com.example.demo.Model.Menu;
import com.example.demo.Model.Order;
import com.example.demo.Model.Voucher;
import com.example.demo.Respository.iCustomerRespository;
import com.example.demo.Respository.iDrinkRespository;
import com.example.demo.Respository.iMenuRespository;
import com.example.demo.Respository.iVoucherRespository;

@RestController
@CrossOrigin
public class AllController {

    // drink
    @Autowired
    iDrinkRespository iDrinkRespository ;

    @GetMapping("drinks")
    public ResponseEntity <List <Drink>> getDrinks() {
        
        try {
            List<Drink> listDrink = new ArrayList<Drink>();
            // object. findAll là hàm tìm kiếm tất cả . forEach và add hết tất cả cho arraylist 
            iDrinkRespository.findAll().forEach(listDrink :: add);

            if(listDrink.size() == 0){
                return new ResponseEntity<List <Drink>>(listDrink, HttpStatus.NOT_FOUND);
            }
            else{
                return new ResponseEntity<List <Drink>>(listDrink, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // voucher 
    @Autowired
    iVoucherRespository repository;

    @GetMapping("vouchers")
    public ResponseEntity <List <Voucher>> getVouchers(){

        try {
            List<Voucher> listVoucher = new ArrayList<Voucher>();
            // object. findAll là hàm tìm kiếm tất cả . forEach và add hết tất cả cho arraylist 
            repository.findAll().forEach(listVoucher :: add);

            if(listVoucher.size() == 0){
                return new ResponseEntity<List <Voucher>>(listVoucher, HttpStatus.NOT_FOUND);
            }
            else{
                return new ResponseEntity<List <Voucher>>(listVoucher, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // menu
    @Autowired
    iMenuRespository iMenu ;

    @GetMapping("menus")
    public ResponseEntity <List <Menu>> getMenu(){

        try {
            List<Menu> listMenu = new ArrayList<Menu>();
            // object. findAll là hàm tìm kiếm tất cả . forEach và add hết tất cả cho arraylist 
            iMenu.findAll().forEach(listMenu :: add);

            if(listMenu.size() == 0){
                return new ResponseEntity<List <Menu>>(listMenu, HttpStatus.NOT_FOUND);
            }
            else{
                return new ResponseEntity<List <Menu>>(listMenu, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // customer + order
    @Autowired
    iCustomerRespository iCustomerRepository;
    @GetMapping("customers")
    public ResponseEntity<List<Customer>> getCustomerList(){
        try{
            List<Customer> listCustomer = new ArrayList<Customer>();

            iCustomerRepository.findAll().forEach(listCustomer::add);
            return new ResponseEntity<List<Customer>>(listCustomer, HttpStatus.OK);
        }catch(Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("orders")
    public ResponseEntity<Set<Order>> getOrderList(@RequestParam (value = "userId") Integer order){
        try{
            Customer vCustomer = iCustomerRepository.findById(order);
            if(vCustomer != null){
                return new ResponseEntity<>(vCustomer.getcOrders(), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        }catch(Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
