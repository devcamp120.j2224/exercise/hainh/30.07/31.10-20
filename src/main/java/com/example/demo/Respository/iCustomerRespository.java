package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Customer;

public interface iCustomerRespository extends JpaRepository <Customer , Long> {
    Customer findById(Integer id);
}
