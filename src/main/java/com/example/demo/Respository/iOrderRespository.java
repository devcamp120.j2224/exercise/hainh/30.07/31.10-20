package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Order;

public interface iOrderRespository extends JpaRepository <Order , Long> {
    
}
