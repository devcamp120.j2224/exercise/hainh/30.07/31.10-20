package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Voucher;

public interface iVoucherRespository extends JpaRepository <Voucher , Long>{
    
}
